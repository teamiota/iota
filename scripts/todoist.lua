-- load required modules
local http = require("ssl.https")
local ltn12 = require("ltn12")
local json = require("cjson")
--require("ansicolors")
local inspect = require("inspect")

gColorset = {0x95ef63, 0xff8581, 0xffc471, 0xf9ec75, 0xa8c8e4, 0xd2b8a3, 0xe2a8e4, 0xcccccc, 0xfb886e, 0xffcc00, 0x74e8d3, 0x3bd5fb, 0xdc4fad, 0xac193d, 0xd24726, 0x82ba00, 0x03b3b2, 0x008299, 0x5db2ff, 0x0072c6, 0x000000, 0x777777}

local myToken = os.getenv("todoistKey")
--local dump = require("dump")

local myUrl='https://todoist.com/API/v7/sync'
--local myUrl="http://httpbin.org/post"
--local myUrl="https://todoist.com/completed/get_stats"

local taskTable = {}

function url_encode(str)
    str = string.gsub (str, "\n", "\r\n")
    str = string.gsub (str, "([^%w%-%_%. ])",
        function (c) return string.format ("%%%02X", string.byte(c)) end)
    return string.gsub (str, " ", "+")
end

function url_formencode(params)
  --table of argument strings
  local argts, i= {}, 1
  for k, v in pairs(params) do
    argts[i]=url_encode(k).."="..url_encode(v)
    i=i+1
  end
  return table.concat(argts,'&')
end

function GetTaskTable()
  if(FileIsThere())then
    local file = io.open("test.txt", "r")
    taskTable = json.decode(file:read())
    file:close()
  else
    print("Somebody is messing with my files")
  end
end

function GetTodoistJson(token)
  local jsonResources = json.encode({"all"})
  local syncToken = "*"
  if (token ~= nil) then
    syncToken = token
  end
  local form_data = url_formencode({resource_types = jsonResources, sync_token = syncToken, token= myToken })

  local headers = {
      ["Content-Type"] = "application/x-www-form-urlencoded";
      ["Content-Length"] = #form_data;
      ["Accept"] = "*/*",
      }

    local rq_resp = {}
    local b,c,h = http.request {
      method = "POST",
      form = form_data,
      url = myUrl,
      headers = headers,
      source = ltn12.source.string(form_data),

      sink = ltn12.sink.table(rq_resp),
    }
  jsonTable = json.decode(table.concat(rq_resp))
  return jsonTable
end

function FileIsThere()
  local file = io.open("test.txt", "r")
  --print(lfs.attributes("test.txt",'mode')
  if (file==nil) then
    return false
  else
    file:close()
    return true
  end

end

function GetInitState()
  if(FileIsThere())then
    GetTaskTable()
    local syncToken = taskTable.sync_token
    local newSync = GetTodoistJson(syncToken)
  else
    local contents = GetTodoistJson()
    local file = io.open("test.txt", "w+")
    taskTable = json.encode(contents)
    file:write(taskTable)
    file:close()
  end
  return taskTable
end

function ListItems()
  GetTaskTable()
  GetKeys(taskTable.items[1])
  for i,v in ipairs(taskTable.items) do
    --if (tonumber(v.checked) ~= 0) then
    local due = v.due_date_utc
    local content = v.content
    if(type(due) == "userdata") then
      due = "No Due Date                   "
    end
    if(v.project_id == 193043579) then
      content = ansicolors.green .. content .. ansicolors.reset
    elseif(v.project_id == 166936470) then
      content = ansicolors.red .. content .. ansicolors.reset
    elseif(v.project_id == 166936472) then
      content = ansicolors.bright..ansicolors.blue .. content .. ansicolors.reset
    elseif(v.project_id == 193210082) then
      content = ansicolors.bright..ansicolors.red .. content .. ansicolors.reset
    elseif(v.project_id == 166936469) then
      content = ansicolors.black..ansicolors.onwhite .. content .. ansicolors.reset
    end
    print(v.date_added, v.checked, due , content)
  --end
  end
end

function ListProjects()
  for i,v in ipairs(taskTable.projects) do
    local content = v.name
    if(v.id == 193043579) then
      content = ansicolors.green .. content .. ansicolors.reset
    elseif(v.id == 166936470) then
      content = ansicolors.bright..ansicolors.red .. content .. ansicolors.reset
    elseif(v.id == 166936472) then
      content = ansicolors.bright..ansicolors.blue .. content .. ansicolors.reset
    elseif(v.id == 193210082) then
      content = ansicolors.red .. content .. ansicolors.reset
    elseif(v.id == 193080004) then
      content = ansicolors.bright..ansicolors.magenta .. content .. ansicolors.reset
    elseif(v.id == 166936469) then
      content = ansicolors.black..ansicolors.onwhite .. content .. ansicolors.reset
    elseif(v.id == 166936471) then
      content = ansicolors.black..ansicolors.bright .. content .. ansicolors.reset
    elseif(v.id == 166936473) then
      content = ansicolors.cyan .. content .. ansicolors.reset
    end
    print(v.indent, v.color, v.id , content)
    --end
  end
end

function GetKeys(keyTable)
  local keyss = ""
  for k,v in pairs(keyTable) do
    keyss = keyss .. ", "..k
  end
  print(string.sub(keyss, 3))
end
