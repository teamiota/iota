require ("todoist")
local inspect = require("inspect")
gColorset = {0x95ef63, 0xff8581, 0xffc471, 0xf9ec75, 0xa8c8e4, 0xd2b8a3, 0xe2a8e4, 0xcccccc, 0xfb886e, 0xffcc00, 0x74e8d3, 0x3bd5fb, 0xdc4fad, 0xac193d, 0xd24726, 0x82ba00, 0x03b3b2, 0x008299, 0x5db2ff, 0x0072c6, 0x000000, 0x777777}

local tasks = {}

function GetProjectColor(id)
  for i,v in ipairs(tasks.projects) do
    if(v.id == id)then
      return gColorset[tonumber(v.color)+1]
    end
  end
end

function PopulateTaskList()
  --GetKeys(tasks.items[1])
  local data = {}
  for i,v in ipairs(tasks.items) do
    --if (tonumber(v.checked) ~= 0) then
    print(i,v.content)
    local due = v.due_date_utc
    local content = v.content
    if(type(due) == "userdata") then
      due = "no due date"
    end
    data["Layer.taskTable.color."..i..".1"] = GetProjectColor(v.project_id)
    data["Layer.taskTable.text."..i..".1"] = v.content .. " - "..due
  end
  gre.set_data(data)
end

function CBInit(mapargs)
  tasks = GetInitState()
  --print(inspect(tasks))
  PopulateTaskList()
end
